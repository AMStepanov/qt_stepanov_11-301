from PySide import QtGui
from PySide.phonon import Phonon

class Window(QtGui.QWidget):
    def __init__(self):
        QtGui.QWidget.__init__(self)
        self.setWindowTitle('Player')
        self.media = Phonon.MediaObject(self)
        self.video = Phonon.VideoWidget(self)
        self.video.setMinimumSize(400, 400)
        self.audio = Phonon.AudioOutput(Phonon.VideoCategory, self)
        Phonon.createPath(self.media, self.audio)
        Phonon.createPath(self.media, self.video)
        self.buttonChoose = QtGui.QPushButton('Choose File', self)
        self.slider = Phonon.VolumeSlider(self)
        self.slider.setAudioOutput(self.audio)
        layout = QtGui.QGridLayout(self)
        layout.addWidget(self.video, 0, 0, 2, 2)
        layout.addWidget(self.buttonChoose, 2, 0)
        layout.addWidget(self.slider, 2, 1, 1, 1)
        layout.setRowStretch(0, 1)
        self.media.stateChanged.connect(self.handleStateChanged)
        self.buttonChoose.clicked.connect(self.handleButtonChoose)

    def handleButtonChoose(self):
        if self.media.state() == Phonon.PlayingState:
            self.media.stop()
        else:
            dialog = QtGui.QFileDialog(self)
            dialog.setFileMode(QtGui.QFileDialog.ExistingFile)
            if dialog.exec_() == QtGui.QDialog.Accepted:
                path = dialog.selectedFiles()[0]
                self.media.setCurrentSource(Phonon.MediaSource(path))
                self.media.play()
            dialog.deleteLater()

    def handleStateChanged(self, newstate, oldstate):
        if newstate == Phonon.PlayingState:
            self.buttonChoose.setText('Stop')
        elif (newstate != Phonon.LoadingState and
              newstate != Phonon.BufferingState):
            self.buttonChoose.setText('Choose File')
            if newstate == Phonon.ErrorState:
                source = self.media.currentSource().fileName()
                print ('ERROR: could not play: %s' % source)
                print ('  %s' % self.media.errorString())


if __name__ == '__main__':

    import sys
    app = QtGui.QApplication(sys.argv)
    app.setApplicationName('Player')
    window = Window()
    window.show()
    sys.exit(app.exec_())