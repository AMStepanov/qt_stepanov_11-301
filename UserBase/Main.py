from PySide.QtGui import *
import sys

from Service import Service


class Main(QWidget):
    def __init__(self):
        super(Main, self).__init__()

        self.setWindowTitle("List of students")
        self.resize(640, 480)
        l1 = QGridLayout()
        self.setLayout(l1)

        self.listItems = QListView(self)
        self.model = QStandardItemModel(self.listItems)
        self.searchField = QLineEdit("", self)

        # Command Buttons
        add_item_button = QPushButton("Add", self)
        search_button = QPushButton("Search", self)
        save_items = QPushButton("Save", self)
        load_items = QPushButton("Load", self)

        # Buttons
        to_start_list = QPushButton("<<", self)
        to_end_list = QPushButton(">>", self)
        to_next_item = QPushButton(">", self)
        to_prev_item = QPushButton("<", self)
        to_delete_item = QPushButton("Delete", self)

        # Add items to widget area
        l1.addWidget(self.searchField, 0, 0, 1, 2)
        l1.addWidget(add_item_button, 1, 0)
        l1.addWidget(search_button, 1, 1)
        l1.addWidget(save_items, 4, 0)
        l1.addWidget(load_items, 4, 1)
        l1.addWidget(self.listItems, 0, 5, 4, 5)
        l1.addWidget(to_start_list, 4, 5, 1, 1)
        l1.addWidget(to_prev_item, 4, 6, 1, 1)
        l1.addWidget(to_next_item, 4, 7, 1, 1)
        l1.addWidget(to_end_list, 4, 8, 1, 1)
        l1.addWidget(to_delete_item, 4, 9, 1, 1)

        # Dependency btw buttons and actions
        add_item_button.clicked.connect(self.addItem)
        search_button.clicked.connect(self.searchItem)
        to_delete_item.clicked.connect(self.deleteItem)
        to_start_list.clicked.connect(self.toStartList)
        to_end_list.clicked.connect(self.toEndList)
        to_next_item.clicked.connect(self.toNextItem)
        to_prev_item.clicked.connect(self.toPrevItem)
        save_items.clicked.connect(self.saveItems)
        load_items.clicked.connect(self.loadItems)

        # Important to show widget
        self.show()

    # Actions for each button
    def addItem(self):
        service = Service(self)
        service.addItem()

    def searchItem(self):
        service = Service(self)
        service.searchItem()

    def deleteItem(self):
        service = Service(self)
        service.deleteItem()

    def toStartList(self):
        service = Service(self)
        service.toStartList()

    def toEndList(self):
        service = Service(self)
        service.toEndList()

    def toNextItem(self):
        service = Service(self)
        service.toNextItem()

    def toPrevItem(self):
        service = Service(self)
        service.toPrevItem()

    def loadItems(self):
        service = Service(self)
        service.loadItems()

    def saveItems(self):
        service = Service(self)
        service.saveItems()

# Main init, add style
if __name__ == "__main__":
    app = QApplication([])
    f = open("style.css", "r")
    app.setStyleSheet(f.read())
    main = Main()

    sys.exit(app.exec_())
