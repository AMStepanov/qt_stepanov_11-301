from PySide.QtGui import *


class Service():
    def __init__(self, main):
        self.main = main

    # To add new item, if field is not empty and list is already not contain item
    def addItem(self):
        if self.main.searchField.text() == "":
            pass
        else:
            f = False
            text = self.main.searchField.text()
            for i in range(self.main.model.rowCount()):
                item = self.main.model.item(i)
                if text == item.text():
                    f = True
                else:
                    f = f or False
            if not f:
                item = QStandardItem(self.main.searchField.text())
                self.main.model.appendRow(item)
                self.main.listItems.setModel(self.main.model)
                self.main.searchField.setText("")

    # Search item, if field is not empty
    def searchItem(self):
        text = self.main.searchField.text()
        if not text == "":
            f = False
            for i in range(self.main.model.rowCount()):
                item = self.main.model.item(i)
                if text == item.text():
                    f = True
                    m = self.main.model.index(i, 0)
                    self.main.listItems.setCurrentIndex(m)
                else:
                    f = f or False

                if not f:
                    m = self.main.model.index(-1, 0)
                    self.main.listItems.setCurrentIndex(m)

    # Simple delete
    def deleteItem(self):
        self.main.model.removeRow(self.main.listItems.currentIndex().row())
        m = self.main.model.index(-1, 0)
        self.main.listItems.setCurrentIndex(m)

    # Go to start
    def toStartList(self):
        m = self.main.model.index(0, 0)
        self.main.listItems.setCurrentIndex(m)

    # Go to end
    def toEndList(self):
        m = self.main.model.index(self.main.model.rowCount()-1, 0)
        self.main.listItems.setCurrentIndex(m)

    def toNextItem(self):
        for i in range(self.main.model.rowCount()):
            if i is self.main.listItems.currentIndex().row():
                if not self.main.listItems.currentIndex().row() == self.main.model.rowCount()-1:
                    m = self.main.model.index(i+1, 0)
                    self.main.listItems.setCurrentIndex(m)
                    break

    def toPrevItem(self):
        for i in range(self.main.model.rowCount()):
            if i is self.main.listItems.currentIndex().row():
                if not self.main.listItems.currentIndex().row() == 0:
                    m = self.main.model.index(i-1, 0)
                    self.main.listItems.setCurrentIndex(m)
                    break

    def saveItems(self):
        f = open("list.txt", "w")
        for i in range(self.main.model.rowCount()):
            item = self.main.model.item(i)
            f.write(item.text()+"\n")
        f.close()

    def loadItems(self):
        self.main.model.clear()
        f = open("list.txt", "r")
        for line in f:
            item = QStandardItem(line[0:-1])
            self.main.model.appendRow(item)
        self.main.listItems.setModel(self.main.model)
        f.close()