import sys
from functools import partial

from PySide.QtCore import QByteArray
from PySide.QtCore import QDataStream
from PySide.QtCore import QIODevice
from PySide.QtCore import SIGNAL
from PySide.QtGui import *
from PySide.QtNetwork import *


class Server(QWidget):
    def __init__(self):
        super(Server, self).__init__()
        self.setWindowTitle("Chat server")
        self.users = []
        self.server = QTcpServer()
        self.server.listen(QHostAddress.Any, 8888)
        self.connect(self.server, SIGNAL("newConnection()"), self.createConnection)

        grid_layout = QGridLayout()
        self.setLayout(grid_layout)
        label_h1 = QLabel("Server", self)
        self.text_box = QTextEdit("", self)
        grid_layout.addWidget(label_h1)
        grid_layout.addWidget(self.text_box)
        self.show()
        self.text_box.append("Сервер запущен...")


    # To create connection
    def createConnection(self):
        connection = self.server.nextPendingConnection()
        connection.nextBlockSize = 0
        connection.disconnected.connect(partial(self.deleteUser, connection))
        connection.readyRead.connect(partial(self.receiveMessage, connection))

    # To delete uses (via stream)
    def deleteUser(self, socket):
        deleted_user = ""
        for u in self.users:
            if u[1] == socket:
                self.users.remove([u[0], socket])
                deleted_user = u[0]
                break
        for u in self.users:
            reply = QByteArray()
            stream = QDataStream(reply, QIODevice.WriteOnly)
            stream.setVersion(QDataStream.Qt_4_2)
            stream.writeUInt32(0)
            stream.writeQString(str(3) + "  " + deleted_user)
            stream.device().seek(0)
            stream.writeUInt32(reply.size() - 8)
            u[1].write(reply)
        self.text_box.append("Пользователь " + deleted_user + " отключился от сервера")


    # Main method to receive messages
    def receiveMessage(self, socket):
        stream = QDataStream(socket)
        if socket.bytesAvailable() > 0:
            stream = QDataStream(socket)

        if socket.nextBlockSize == 0:
            if socket.bytesAvailable() < 8:
                return
        socket.nextBlockSize = stream.readUInt32()
        if socket.bytesAvailable() < socket.nextBlockSize:
            return

        textFromClient = stream.readQString()
        socket.nextBlockSize = 0

        l = textFromClient.split("  ")

        # System message
        if l[0] == "0":
            f = False
            for u in self.users:
                if u[0] == l[1]:
                    f = True

            if f:
                self.users.append([l[1], socket])
                reply = QByteArray()
                stream = QDataStream(reply, QIODevice.WriteOnly)
                stream.setVersion(QDataStream.Qt_4_2)
                stream.writeUInt32(0)
                stream.writeQString(str(2))
                stream.device().seek(0)
                stream.writeUInt32(reply.size() - 8)
                socket.write(reply)
            else:
                self.users.append([l[1], socket])
                for u in self.users:
                    reply = QByteArray()
                    stream = QDataStream(reply, QIODevice.WriteOnly)
                    stream.setVersion(QDataStream.Qt_4_2)
                    stream.writeUInt32(0)
                    stream.writeQString(str(0) + "  " + "Пользователь " + l[1] + " присоединился к чату")
                    stream.device().seek(0)
                    stream.writeUInt32(reply.size() - 8)
                    u[1].write(reply)
                self.text_box.append("Пользователь " + l[1] + " присоединился к серверу")
                # print("Пользователь " + l[1] + " присоединился к серверу")


        # Message from user
        elif l[0] == "1":
            user = ""
            for u in self.users:
                if u[1] == socket:
                    user = u[0]

            #print(user + ": " + l[1])
            self.text_box.append(user + ": " + l[1])


            for u in self.users:
                reply = QByteArray()
                stream = QDataStream(reply, QIODevice.WriteOnly)
                stream.setVersion(QDataStream.Qt_4_2)
                stream.writeUInt32(0)
                stream.writeQString(str(1) + "  " + user + ": " + l[1])
                stream.device().seek(0)
                stream.writeUInt32(reply.size() - 8)
                u[1].write(reply)

if __name__ == "__main__":

    app = QApplication([])
    f = open("style.css", "r")
    app.setStyleSheet(f.read())
    server = Server()

    sys.exit(app.exec_())