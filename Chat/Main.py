import sys
from functools import partial

from PySide.QtCore import QByteArray, SIGNAL, SLOT
from PySide.QtCore import QDataStream
from PySide.QtCore import QIODevice
from PySide.QtGui import *
from PySide.QtNetwork import *

class Main(QWidget):
    def __init__(self):
        super(Main, self).__init__()
        self.setWindowTitle("Chat")
        self.socket = QTcpSocket()
        self.socket.connectToHost("127.0.0.1", 8888)
        self.list_users = []
        self.grid_layout = QGridLayout()
        self.setLayout(self.grid_layout)
        self.line_edit_login = QLineEdit("Логин", self)
        self.line_edit_text = QLineEdit("Текст сообщения", self)
        self.line_edit_text.hide()
        self.send_message = QPushButton("Отправить", self)
        self.send_message.hide()
        self.label_h1 = QLabel("Client", self)
        self. text_box = QTextEdit("", self)
        self.connect_button = QPushButton("Подключиться", self)
        self.grid_layout.addWidget(self.label_h1)
        self.grid_layout.addWidget(self.text_box)
        self.grid_layout.addWidget(self.line_edit_text)
        self.grid_layout.addWidget(self.line_edit_login)
        self.grid_layout.addWidget(self.connect_button)
        self.show()
        # self.connect(self.socket, SIGNAL("disconnected()"), self.disconnected())
        self.connect_button.clicked.connect(self.login)
        self.send_message.clicked.connect(self.sendMessage)
        self.socket.readyRead.connect(partial(self.receiveMessage, self.socket))
        self.connect(self.socket, SIGNAL("newConnection()"), self.createConnection)

    def login(self):
        reply = QByteArray()
        stream = QDataStream(reply, QIODevice.WriteOnly)
        stream.setVersion(QDataStream.Qt_4_2)
        stream.writeUInt32(0)
        stream.writeQString(str(0) + "  " +  self.line_edit_login.text())
        stream.device().seek(0)
        stream.writeUInt32(reply.size() - 8)
        self.socket.write(reply)

    def createConnection(self):
        connection = self.socket.nextPendingConnection()
        connection.nextBlockSize = 0
        connection.readyRead.connect(partial(self.receiveMessage, connection))

    def receiveMessage(self, socket):
        socket.nextBlockSize = 0
        stream = QDataStream(socket)
        if socket.bytesAvailable() > 0:
            stream = QDataStream(socket)

        if socket.nextBlockSize == 0:
            if socket.bytesAvailable() < 8:
                return
        socket.nextBlockSize = stream.readUInt32()
        if socket.bytesAvailable() < socket.nextBlockSize:
            return

        textFromClient = stream.readQString()
        l = textFromClient.split("  ")
        if l[0] == "0":
            socket.nextBlockSize = 0
            self.text_box.append(l[1])
            self.connect_button.hide()
            self.line_edit_login.hide()
            self.line_edit_text.show()
            self.label_h1.setText(self.line_edit_login.text())
            self.send_message.show()
            self.grid_layout.addWidget(self.send_message)

        elif l[0] == "1":
            text = ""
            for i in range(1, len(l)):
                text += l[i] + " "
            self.text_box.append(text)

        elif l[0] == "2":
            self.label_h1.setText("Такой пользователь уже есть")

        elif l[0] == "3":
            self.text_box.append("Пользователь " + l[1] + " вышел из чата...")

    def sendMessage(self):
        if self.line_edit_text.text() == "":
            pass
        else:
            text = str(1) + "   " + self.line_edit_text.text()

            reply = QByteArray()
            stream = QDataStream(reply, QIODevice.WriteOnly)
            stream.setVersion(QDataStream.Qt_4_2)
            stream.writeUInt32(0)
            stream.writeQString(text)
            stream.device().seek(0)
            stream.writeUInt32(reply.size() - 8)
            self.socket.write(reply)
            self.line_edit_text.clear()

if __name__ == "__main__":

    app = QApplication([])
    f = open("style.css", "r")
    app.setStyleSheet(f.read())
    main = Main()

    sys.exit(app.exec_())
