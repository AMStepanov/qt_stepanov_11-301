import sympy

# Calculation
class Tools():
    def count(self, string):
        try:
            result = sympy.sympify(string)
        except sympy.SympifyError:
            result = "Incorrect expression"
        return str("Result: " + str(result))