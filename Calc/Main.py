import sys

from PySide.QtGui import *

from Tools import Tools


class Main(QWidget):
    def __init__(self):
        super(Main, self).__init__()
        self.setWindowTitle("Calculator")
        self.resize(200, 200)
        l1 = QGridLayout()
        self.setLayout(l1)
        self.le = QLineEdit("", self)
        self.lbl = QLabel("", self)

        # Buttons
        btn1 = QPushButton("1", self)
        btn2 = QPushButton("2", self)
        btn3 = QPushButton("3", self)
        btn4 = QPushButton("4", self)
        btn5 = QPushButton("5", self)
        btn6 = QPushButton("6", self)
        btn7 = QPushButton("7", self)
        btn8 = QPushButton("8", self)
        btn9 = QPushButton("9", self)
        btn0 = QPushButton("0", self)
        btnC = QPushButton("C", self)
        btnDot = QPushButton(".", self)
        btnCount = QPushButton("=", self)
        btnOpen = QPushButton("(", self)
        btnClose = QPushButton(")", self)
        btnPlus = QPushButton("+", self)
        btnMinus = QPushButton("-", self)
        btnDiv = QPushButton("/", self)
        btnMult = QPushButton("*", self)

        # Add items to widget area
        l1.addWidget(self.le, 0, 0, 1, 4)
        l1.addWidget(self.lbl, 1, 0, 1, 4)
        l1.addWidget(btn1)
        l1.addWidget(btn2)
        l1.addWidget(btn3)
        l1.addWidget(btnPlus)
        l1.addWidget(btn4)
        l1.addWidget(btn5)
        l1.addWidget(btn6)
        l1.addWidget(btnMinus)
        l1.addWidget(btn7)
        l1.addWidget(btn8)
        l1.addWidget(btn9)
        l1.addWidget(btnDiv)
        l1.addWidget(btn0)
        l1.addWidget(btnC)
        l1.addWidget(btnCount)
        l1.addWidget(btnMult)
        l1.addWidget(btnOpen)
        l1.addWidget(btnClose)
        l1.addWidget(btnDot)

        # Dependency btw buttons and actions
        btn1.clicked.connect(self.addBtn1)
        btn2.clicked.connect(self.addBtn2)
        btn3.clicked.connect(self.addBtn3)
        btn4.clicked.connect(self.addBtn4)
        btn5.clicked.connect(self.addBtn5)
        btn6.clicked.connect(self.addBtn6)
        btn7.clicked.connect(self.addBtn7)
        btn8.clicked.connect(self.addBtn8)
        btn9.clicked.connect(self.addBtn9)
        btn0.clicked.connect(self.addBtn0)
        btnC.clicked.connect(self.addBtnC)
        btnDot.clicked.connect(self.addBtnDot)
        btnOpen.clicked.connect(self.addBtnOpen)
        btnClose.clicked.connect(self.addBtnClose)
        btnPlus.clicked.connect(self.addBtnPlus)
        btnMinus.clicked.connect(self.addBtnMinus)
        btnDiv.clicked.connect(self.addBtnDiv)
        btnMult.clicked.connect(self.addBtnMult)
        btnCount.clicked.connect(self.addBtnCount)

        # Important to show widget
        self.show()

    # Actions for each button
    def addBtn1(self):
        self.le.setText(self.le.text()+"1")

    def addBtn2(self):
        self.le.setText(self.le.text()+"2")

    def addBtn3(self):
        self.le.setText(self.le.text()+"3")

    def addBtn4(self):
        self.le.setText(self.le.text()+"4")

    def addBtn5(self):
        self.le.setText(self.le.text()+"5")

    def addBtn6(self):
        self.le.setText(self.le.text()+"6")

    def addBtn7(self):
        self.le.setText(self.le.text()+"7")

    def addBtn8(self):
        self.le.setText(self.le.text()+"8")

    def addBtn9(self):
        self.le.setText(self.le.text()+"9")

    def addBtn0(self):
        self.le.setText(self.le.text()+"0")

    def addBtnC(self):
        self.le.setText("")

    def addBtnDot(self):
        self.le.setText(self.le.text()+".")

    def addBtnOpen(self):
        self.le.setText(self.le.text()+"(")

    def addBtnClose(self):
        self.le.setText(self.le.text()+")")

    def addBtnPlus(self):
        self.le.setText(self.le.text()+"+")

    def addBtnMinus(self):
        self.le.setText(self.le.text()+"-")

    def addBtnDiv(self):
        self.le.setText(self.le.text()+"/")

    def addBtnMult(self):
        self.le.setText(self.le.text()+"*")

    def addBtnCount(self):
        tools = Tools()
        result = tools.count(self.le.text())
        self.lbl.setText(result)

# Main init
if __name__ == "__main__":
    app = QApplication([])
    f = open("style.css", "r")
    app.setStyleSheet(f.read())
    main = Main()

    sys.exit(app.exec_())