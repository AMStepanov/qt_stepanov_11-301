from PySide import QtCore


# For Status Bar info
class Communicate(QtCore.QObject):

    msgToSB = QtCore.Signal(str)
