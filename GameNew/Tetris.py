from PySide import QtCore, QtGui
from Board import Board


# Initial class
class Tetris(QtGui.QMainWindow):

    def __init__(self):
        super(Tetris, self).__init__()

        # Window size, title, board
        self.setGeometry(300, 300, 180, 380)
        self.setWindowTitle('Tetris')
        self.Tetrisboard = Board(self)

        self.setCentralWidget(self.Tetrisboard)

        # Status Bar
        self.statusbar = self.statusBar()
        self.Tetrisboard.c.msgToSB[str].connect(self.statusbar.showMessage)

        # Start board, place to centre
        self.Tetrisboard.start()
        self.center()

    # To find centre of screen using screen.width and screen.height
    def center(self):

        screen = QtGui.QDesktopWidget().screenGeometry()
        size = self.geometry()
        self.move((screen.width()-size.width())/2,
            (screen.height()-size.height())/2)

