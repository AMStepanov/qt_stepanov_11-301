
from PySide.QtGui import *
import sys
from Tetris import Tetris


class Main(QWidget):
    def __init__(self):
        super(Main, self).__init__()

        self.setWindowTitle("Tetris")
        self.resize(300, 100)
        l1 = QGridLayout()
        self.setLayout(l1)

        # Buttons
        singleplayer_button = QPushButton("Single Player Game", self)
        highscore_button = QPushButton("Highscores", self)
        exit_button = QPushButton("Exit", self)

        # Add items to widget area
        l1.addWidget(singleplayer_button, 0, 0)
        l1.addWidget(highscore_button, 1, 0)
        l1.addWidget(exit_button, 2, 0)

        # Dependency btw buttons and actions
        singleplayer_button.clicked.connect(self.singleplayer_game)
        highscore_button.clicked.connect(self.show_highscores)
        exit_button.clicked.connect(self.exit_application)

        # Important to show widget
        self.show()

    # Actions for each button
    def singleplayer_game(self):
        tetris = Tetris()
        tetris.show()
        tetris.exec_()

    def show_highscores(self):
        print("TODO")
        # highscores = Highscores(self)
        # highscores.exec_()


    def exit_application(self):
         if QMessageBox.question(None, '', "Do you really want to exit?", QMessageBox.Yes | QMessageBox.No, QMessageBox.No) == QMessageBox.Yes:QApplication.quit()

    def closeEvent(self, event):
        if QMessageBox.question(None, '', "Do you really want to exit?", QMessageBox.Yes | QMessageBox.No, QMessageBox.No) == QMessageBox.Yes:QApplication.quit()
        else: event.ignore()

# Main init, add style
if __name__ == "__main__":
    app = QApplication([])
    f = open("style.css", "r")
    app.setStyleSheet(f.read())
    main = Main()

    sys.exit(app.exec_())
